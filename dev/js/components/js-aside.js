$(document).ready(function() {
    $('.js-menu-resizer').on('click', function() {
        $(this).toggleClass('is-active');
        $(this).closest('.nav-menu').toggleClass('is-opened');
    });

    $('.js-menu-trigger').on('click', function() {
        $('.nav-menu__list-wrap').addClass('is-active');
    });

    $('.js-menu-close').on('click', function() {
        $('.nav-menu__list-wrap').removeClass('is-active');
    });
});