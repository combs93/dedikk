$(document).ready(function() {
    $('.js-status-popup-trigger').on('click', function() {
        $('.status-popup').fadeIn(300);
    });
    
    $('.status-popup__close').on('click', function() {
        $('.status-popup').fadeOut(300);
    });
});