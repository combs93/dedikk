$(document).ready(function() {
    $('.tab-triggers__item').on('click', function() {
        var $this = $(this),
            $tabName = $this.data('tab'),
            $parent = $this.closest('.tabs-wrapper');
            
        if ($this.hasClass('is-active')) {
            return
        }

        $this.siblings('.tab-triggers__item.is-active').removeClass('is-active');
        $this.addClass('is-active');

       $parent.find('.tab-pane.is-active').hide().removeClass('is-active');
       $parent.find('.tab-pane#'+$tabName).fadeIn(300).addClass('is-active');
    });
});