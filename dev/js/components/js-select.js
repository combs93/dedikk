$(document).ready(function() {
    $('.custom-select').each(function() {
        var $this = $(this),
            $selectedLi = $this.find('li.selected');
        if ($selectedLi.length) {
            var text = $selectedLi.text(),
                value = $selectedLi.data('value');
            $this.find('.custom-select-trigger').attr('data-selected', value);
            $this.find('.custom-select-trigger__text').text(text)
        }
    })

    $('.custom-select').on('click', '.custom-select-trigger', function() {
        $(this).closest('.custom-select').toggleClass('is-active');
    });

    $('.custom-select').on('click', 'li', function() {
        var $this = $(this),
            selectedText = $this.text(),
            value = $this.data('value'),
            parent = $this.closest('.custom-select'),
            trigger = parent.find('.custom-select-trigger');
        
        $this.siblings('.selected').removeClass('selected');
        $this.addClass('selected');
        trigger.find('.custom-select-trigger__text').text(selectedText);
        trigger.attr('data-selected', value);
        parent.removeClass('is-active');
    });

    $('body').on('click', function(e){
        if ($(e.target).closest('.custom-select').length === 0) {
            $('.custom-select').removeClass('is-active');
        }
    });
});