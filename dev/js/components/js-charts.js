$(document).ready(function() {
    
    // circle chart
    if ($('.js-circle-chart').length) {
        Chart.defaults.global.defaultFontFamily = "'DM Sans', sans-serif";
        
        var charts_js_circle = $('.js-circle-chart canvas');
        
        charts_js_circle.each(function(ind, el) {
            var ctc = el.getContext('2d');

            var loaded = JSON.parse($(el).attr('data-loaded'));
            var loadedText = JSON.parse($(el).attr('data-loaded-text'));
            var approved = JSON.parse($(el).attr('data-approved'));
            var approvedText = JSON.parse($(el).attr('data-approved-text'));
            
            var circleChart = new Chart(ctc, {
                type: 'doughnut',
                data: {
                    datasets: [
                        {
                            data: loaded,
                            backgroundColor: [
                                'rgba(83, 110, 220, 1)',
                                '#E8EDF2',
                            ],
                            hoverBackgroundColor: [
                                'rgba(83, 110, 220, 1)',
                                '#E8EDF2',
                            ],
                            hoverBorderColor: 'rgba(255,255,255,1)',
                            borderColor: [
                                '#ffffff'
                            ],
                            borderWidth: 4,
                            weight: 2,
                        },
                        {
                            data: approved,
                            backgroundColor: [
                                'rgba(71, 218, 121, 1)',
                                '#E8EDF2',
                            ],
                            hoverBackgroundColor: [
                                'rgba(71, 218, 121, 1)',
                                '#E8EDF2',
                            ],
                            hoverBorderColor: 'rgba(255,255,255,1)',
                            borderColor: [
                                '#ffffff'
                            ],
                            borderWidth: 4,
                            weight: 2
                        }
                    ]
                },
                options: {
                    responsive: true,
                    cutoutPercentage: 70,
                    roundedCorners: true,
                    elements: {
                        arc: {
                            borderWidth: 30
                        }
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            display: false
                        }],
                        xAxes: [{
                            display: false
                        }]
                    },
                    tooltips: {
                        enabled: false
                    },
                    plugins: {
                        doughnutlabel: {
                            labels: [
                                {
                                    text: loadedText,
                                    font: {
                                        size: '16',
                                        family: "'DM Sans', sans-serif",
                                        weight: '600',
                                        lineHeight: '1.5'
                                    },
                                    color: '#536EDC'
                                },
                                {
                                    text: approvedText,
                                    font: {
                                        size: '16',
                                        family: "'DM Sans', sans-serif",
                                        weight: '600',
                                        lineHeight: '1.5'
                                    },
                                    color: '#47DA79'
                                },
                            ]
                        }
                    }
                }
            });
        });

    }

    // vertical charts
    if ($('.js-vertical-chart').length) {
        Chart.defaults.global.defaultFontFamily = "'DM Sans', sans-serif";

        var charts_js_vertical = $('.js-vertical-chart canvas');

        charts_js_vertical.each(function(ind, el) {

            var ctv = el.getContext('2d');

            var loaded = JSON.parse($(el).attr('data-loaded'));
            var approved = JSON.parse($(el).attr('data-approved'));
            var commonLoadedArr = loaded.concat(approved);

            var borderColorsArr = [];

            var groupTitles = JSON.parse($(el).attr('data-groupTitles'));

            var verticalMinValue = 0;
            var verticalMaxValue = 0;

            //CHECK MAX VALUE

            for (var i = 0; i < commonLoadedArr.length; i++) {
                (function(i) {
                    if(commonLoadedArr[i] > verticalMaxValue) {
                        verticalMaxValue = commonLoadedArr[i];
                    }
                })(i);
            }
            
            for (var i = 0; i < loaded.length; i++) {
                borderColorsArr.push('#ffffff');
            }

            //CHECK MAX VALUE  END

            new Chart(ctv, {
                type: 'bar',
                data: {
                    labels: groupTitles,
                    datasets: [
                        {
                            label: 'Загружено',
                            backgroundColor: 'rgba(83, 110, 220, 1)',
                            hoverBackgroundColor: 'rgba(83, 110, 220, 1)',
                            borderColor: borderColorsArr,
                            data: loaded,
                            barPercentage: 0.8,
                            categoryPercentage: 0.3,
                        },
                        {
                            label: 'Подошли',
                            backgroundColor: 'rgba(71, 218, 121, 1)',
                            hoverBackgroundColor: 'rgba(71, 218, 121, 1)',
                            borderColor: borderColorsArr,
                            data: approved,
                            barPercentage: 0.8,
                            categoryPercentage: 0.3,
                        }
                    ]
                },
                options: {
                    responsive: true,
                    cornerRadius: 2,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }
                    },
                    tooltips: {
                        backgroundColor: '#F5F5F7',
                        cornerRadius: 8,
                        bodyFontColor: '#383838',
                        bodyFontStyle: 'bold',
                        bodyFontSize: 14,
                        bodyFontFamily: "'DM Sans', sans-serif",
                        caretSize: 0,
                        yPadding: 8,
                        xPadding: 8,
                        callbacks: {
                            title: function() {
                                return ''
                            }
                        }
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            gridLines: {
                                color: "#F5F5F7",
                                zeroLineColor: '#F5F5F7',
                                drawTicks: false,
                                drawBorder: false
                            },
                            ticks: {
                                padding: 0,
                                fontSize: 14,
                                fontColor: '#6F6E6E',
                                stepSize: 10,
                                callback: function(value, index, labels) {
                                    return value;
                                }
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false,
                                drawTicks: false,
                                color: 'transparent',
                                drawBorder: false
                            },
                            ticks: {
                                padding: 33,
                                fontColor: '#6F6E6E',
                                fontSize: 14,
                                callback: function(label, index, labels) {
                                    return label;
                                }
                            }
                        }]
                    }
                }
            });
        });
    }
});