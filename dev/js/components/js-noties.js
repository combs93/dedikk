$(document).ready(function() {
    $('.js-noties-trigger').on('click', function() {
        $('.noties-wrapper').toggleClass('is-active');
    });
    $('.js-noties-close').on('click', function() {
        $('.noties-wrapper').removeClass('is-active');
    });
    $('.js-noties-delete').on('click', function() {
        $(this).closest('.noties-item-wrap').remove();
        if ($('.noties-item-wrap').length < 1) {
            $('.noties-wrapper').remove();
        }
    });
    $('.js-scrollbar').scrollbar();
});