$(document).ready(function() {
    $('#file-load__input').on('change', function() {
        $('.file-load__text').text($(this).get(0).files[0].name);
        $(this).closest('.file-load').find('.btn').removeAttr('disabled');
    });
});