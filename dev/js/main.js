/* frameworks */
//=include ../../node_modules/jquery/dist/jquery.min.js
//=include ../../node_modules/chart.js/dist/Chart.min.js
//=include lib/Chart.roundedBarCharts.min.js
//=include lib/doughnutlabel.js

/* libs */
//=include lib/modernizr-custom.js
//=include lib/jquery.scrollbar.min.js

/* plugins */

/* separate */
//=include helpers/object-fit.js
//=include separate/global.js

/* components */
//=include components/js-aside.js
//=include components/js-noties.js
//=include components/js-select.js
//=include components/js-charts.js
//=include components/js-tabs.js
//=include components/js-file-input.js
//=include components/js-popup.js

// the main code
